# Leaflet Map annotation and Geolocation App with Angular12 and Ionic6

Plugins utilized:

npm install cordova-plugin-geolocation

npm install @ionic-native/geolocation

npm install –-save @ionic-native/core@latest

npm install leaflet --save

npm install leaflet-ant-path --save

ionic cap sync

# Notes

https://stackblitz.com/ 

# Links
https://leafletjs.com/
https://www.openstreetmap.org/
https://www.mapbox.com/
https://ionicframework.com
https://angularjs.com


# Author
Qualquer dúvida:
IG: https://www.instagram.com/luiz.alexandre013/
Facebook: https://www.facebook.com/profluizalexandre/

# Authors 2022 
Greg Willson
https://biomassiv.es
https://scdhub.org  

Bonface Murithi
