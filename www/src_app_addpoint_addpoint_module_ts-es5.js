(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkGround_Cover_Mappper"] = self["webpackChunkGround_Cover_Mappper"] || []).push([["src_app_addpoint_addpoint_module_ts"], {
    /***/
    97795:
    /*!*****************************************************!*\
      !*** ./src/app/addpoint/addpoint-routing.module.ts ***!
      \*****************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AddpointPageRoutingModule": function AddpointPageRoutingModule() {
          return (
            /* binding */
            _AddpointPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _addpoint_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./addpoint.page */
      16897);

      var routes = [{
        path: '',
        component: _addpoint_page__WEBPACK_IMPORTED_MODULE_0__.AddpointPage
      }];

      var _AddpointPageRoutingModule = /*#__PURE__*/_createClass(function AddpointPageRoutingModule() {
        _classCallCheck(this, AddpointPageRoutingModule);
      });

      _AddpointPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _AddpointPageRoutingModule);
      /***/
    },

    /***/
    28211:
    /*!*********************************************!*\
      !*** ./src/app/addpoint/addpoint.module.ts ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AddpointPageModule": function AddpointPageModule() {
          return (
            /* binding */
            _AddpointPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _addpoint_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./addpoint-routing.module */
      97795);
      /* harmony import */


      var _addpoint_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./addpoint.page */
      16897);

      var _AddpointPageModule = /*#__PURE__*/_createClass(function AddpointPageModule() {
        _classCallCheck(this, AddpointPageModule);
      });

      _AddpointPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _addpoint_routing_module__WEBPACK_IMPORTED_MODULE_0__.AddpointPageRoutingModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule],
        declarations: [_addpoint_page__WEBPACK_IMPORTED_MODULE_1__.AddpointPage]
      })], _AddpointPageModule);
      /***/
    },

    /***/
    16897:
    /*!*******************************************!*\
      !*** ./src/app/addpoint/addpoint.page.ts ***!
      \*******************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AddpointPage": function AddpointPage() {
          return (
            /* binding */
            _AddpointPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_addpoint_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./addpoint.page.html */
      8790);
      /* harmony import */


      var _addpoint_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./addpoint.page.scss */
      76874);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      3679);

      var _AddpointPage = /*#__PURE__*/function () {
        function AddpointPage(formBuilder) {
          _classCallCheck(this, AddpointPage);

          this.formBuilder = formBuilder;
          this.defaultDate = "2020-04-01";
          this.isSubmitted = false;
        }

        _createClass(AddpointPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.ionicForm = this.formBuilder.group({
              label: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.minLength(2)]],
              dob: [this.defaultDate],
              mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.pattern('^[0-9]+$')]]
            });
          }
        }, {
          key: "getDate",
          value: function getDate(e) {
            var date = new Date(e.target.value).toISOString().substring(0, 10);
            this.ionicForm.get('dob').setValue(date, {
              onlyself: true
            });
          }
        }, {
          key: "errorControl",
          get: function get() {
            return this.ionicForm.controls;
          }
        }, {
          key: "submitForm",
          value: function submitForm() {
            this.isSubmitted = true;

            if (!this.ionicForm.valid) {
              console.log('Please provide all the required values!');
              return false;
            } else {
              console.log(this.ionicForm.value);
              /*  this.ionicForm.reset(); */
            }
          }
        }]);

        return AddpointPage;
      }();

      _AddpointPage.ctorParameters = function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormBuilder
        }];
      };

      _AddpointPage = (0, tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-addpoint',
        template: _raw_loader_addpoint_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_addpoint_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _AddpointPage);
      /***/
    },

    /***/
    76874:
    /*!*********************************************!*\
      !*** ./src/app/addpoint/addpoint.page.scss ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGRwb2ludC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    8790:
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/addpoint/addpoint.page.html ***!
      \***********************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons collapse=\"true\" slot=\"end\">\n      <ion-button href=\"/addpoint\">\n        <ion-icon name=\"locate\" color=\"warning\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/polylines\">\n        <ion-icon name=\"analytics\" alt=\"Polylines\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/home\">\n        <ion-icon name=\"map\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/settings\">\n        <ion-icon name=\"settings-outline\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/profile\">\n        <ion-icon size=\"large\" name=\"person-circle\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Ground Cover Mapper\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-card>\n    <ion-card-header>\n      <ion-card-title>Add data</ion-card-title>\n    </ion-card-header>\n  </ion-card>\n\n  <form [formGroup]=\"ionicForm\" (ngSubmit)=\"submitForm()\" novalidate>\n    <ion-item lines=\"full\">\n      <ion-label position=\"floating\">Label</ion-label>\n      <ion-input formControlName=\"label\" type=\"text\"></ion-input>\n    </ion-item>\n    <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.label.errors?.required\">\n      Label is required.\n    </span>\n    <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.label.errors?.minlength\">\n      Label should be min 2 chars long.\n    </span>\n\n    <ion-item lines=\"full\">\n      <ion-label position=\"floating\">Date of obsevation</ion-label>\n      <ion-datetime (ionChange)=\"getDate($event)\" formControlName=\"dob\" [value]=\"defaultDate\"></ion-datetime>\n    </ion-item>\n    <ion-item lines=\"full\">\n      <ion-label position=\"floating\">Notes</ion-label>\n      <ion-input maxlength=\"10\" formControlName=\"mobile\" type=\"text\" required></ion-input>\n    </ion-item>\n    <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.mobile.errors?.required\">\n      Mobile number is required.\n    </span>\n    <span class=\"error ion-padding\" *ngIf=\"isSubmitted && errorControl.mobile.errors?.pattern\">\n      Only numerical values allowed.\n    </span>\n    <ion-row>\n      <ion-col>\n        <ion-button type=\"submit\" color=\"primary\" expand=\"block\">Submit</ion-button>\n      </ion-col>\n    </ion-row>\n  </form>\n</ion-content>\n\n\n\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_addpoint_addpoint_module_ts-es5.js.map