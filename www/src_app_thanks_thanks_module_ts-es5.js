(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkGround_Cover_Mappper"] = self["webpackChunkGround_Cover_Mappper"] || []).push([["src_app_thanks_thanks_module_ts"], {
    /***/
    32927:
    /*!*************************************************!*\
      !*** ./src/app/thanks/thanks-routing.module.ts ***!
      \*************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "ThanksPageRoutingModule": function ThanksPageRoutingModule() {
          return (
            /* binding */
            _ThanksPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _thanks_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./thanks.page */
      56233);

      var routes = [{
        path: '',
        component: _thanks_page__WEBPACK_IMPORTED_MODULE_0__.ThanksPage
      }];

      var _ThanksPageRoutingModule = /*#__PURE__*/_createClass(function ThanksPageRoutingModule() {
        _classCallCheck(this, ThanksPageRoutingModule);
      });

      _ThanksPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _ThanksPageRoutingModule);
      /***/
    },

    /***/
    67694:
    /*!*****************************************!*\
      !*** ./src/app/thanks/thanks.module.ts ***!
      \*****************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "ThanksPageModule": function ThanksPageModule() {
          return (
            /* binding */
            _ThanksPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _thanks_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./thanks-routing.module */
      32927);
      /* harmony import */


      var _thanks_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./thanks.page */
      56233);

      var _ThanksPageModule = /*#__PURE__*/_createClass(function ThanksPageModule() {
        _classCallCheck(this, ThanksPageModule);
      });

      _ThanksPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _thanks_routing_module__WEBPACK_IMPORTED_MODULE_0__.ThanksPageRoutingModule],
        declarations: [_thanks_page__WEBPACK_IMPORTED_MODULE_1__.ThanksPage]
      })], _ThanksPageModule);
      /***/
    },

    /***/
    56233:
    /*!***************************************!*\
      !*** ./src/app/thanks/thanks.page.ts ***!
      \***************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "ThanksPage": function ThanksPage() {
          return (
            /* binding */
            _ThanksPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_thanks_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./thanks.page.html */
      89919);
      /* harmony import */


      var _thanks_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./thanks.page.scss */
      17828);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);

      var _ThanksPage = /*#__PURE__*/function () {
        function ThanksPage() {
          _classCallCheck(this, ThanksPage);
        }

        _createClass(ThanksPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ThanksPage;
      }();

      _ThanksPage.ctorParameters = function () {
        return [];
      };

      _ThanksPage = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-thanks',
        template: _raw_loader_thanks_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_thanks_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _ThanksPage);
      /***/
    },

    /***/
    17828:
    /*!*****************************************!*\
      !*** ./src/app/thanks/thanks.page.scss ***!
      \*****************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0aGFua3MucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    89919:
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/thanks/thanks.page.html ***!
      \*******************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>thanks</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_thanks_thanks_module_ts-es5.js.map