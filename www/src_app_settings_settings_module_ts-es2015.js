(self["webpackChunkGround_Cover_Mappper"] = self["webpackChunkGround_Cover_Mappper"] || []).push([["src_app_settings_settings_module_ts"],{

/***/ 91836:
/*!*****************************************************!*\
  !*** ./src/app/settings/settings-routing.module.ts ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsPageRoutingModule": function() { return /* binding */ SettingsPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settings.page */ 7162);




const routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_0__.SettingsPage
    }
];
let SettingsPageRoutingModule = class SettingsPageRoutingModule {
};
SettingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SettingsPageRoutingModule);



/***/ }),

/***/ 27075:
/*!*********************************************!*\
  !*** ./src/app/settings/settings.module.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsPageModule": function() { return /* binding */ SettingsPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _settings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settings-routing.module */ 91836);
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.page */ 7162);







let SettingsPageModule = class SettingsPageModule {
};
SettingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _settings_routing_module__WEBPACK_IMPORTED_MODULE_0__.SettingsPageRoutingModule
        ],
        declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_1__.SettingsPage]
    })
], SettingsPageModule);



/***/ }),

/***/ 7162:
/*!*******************************************!*\
  !*** ./src/app/settings/settings.page.ts ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsPage": function() { return /* binding */ SettingsPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_settings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./settings.page.html */ 14718);
/* harmony import */ var _settings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.page.scss */ 69519);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);




let SettingsPage = class SettingsPage {
    constructor() {
    }
    ngOnInit() {
    }
};
SettingsPage.ctorParameters = () => [];
SettingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-settings',
        template: _raw_loader_settings_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_settings_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SettingsPage);



/***/ }),

/***/ 69519:
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.scss ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toggle::before {\n  position: absolute;\n  top: 16px;\n  left: 10px;\n  content: \"ON\";\n  color: white;\n  font-size: 8px;\n  z-index: 1;\n}\n\nion-toggle {\n  --background: #000;\n  --background-checked: #7a49a5;\n  --handle-background: #7a49a5;\n  --handle-background-checked: #000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBRUEsU0FBQTtFQUNBLFVBQUE7RUFFQSxhQUFBO0VBRUEsWUFBQTtFQUNBLGNBQUE7RUFFQSxVQUFBO0FBSEo7O0FBS0U7RUFDRSxrQkFBQTtFQUNBLDZCQUFBO0VBRUEsNEJBQUE7RUFDQSxpQ0FBQTtBQUhKIiwiZmlsZSI6InNldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b2dnbGU6OmJlZm9yZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICBcbiAgICB0b3A6IDE2cHg7XG4gICAgbGVmdDogMTBweDtcbiAgXG4gICAgY29udGVudDogXCJPTlwiO1xuICBcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiA4cHg7XG4gIFxuICAgIHotaW5kZXg6IDE7XG4gIH1cbiAgaW9uLXRvZ2dsZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwO1xuICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjN2E0OWE1O1xuICBcbiAgICAtLWhhbmRsZS1iYWNrZ3JvdW5kOiAjN2E0OWE1O1xuICAgIC0taGFuZGxlLWJhY2tncm91bmQtY2hlY2tlZDogIzAwMDtcbiAgfSJdfQ== */");

/***/ }),

/***/ 14718:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html ***!
  \***********************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("        <ion-header [translucent]=\"true\">\n          <ion-toolbar>\n            <ion-buttons collapse=\"true\" slot=\"end\">\n              <ion-button href=\"/addpoint\">\n                <ion-icon name=\"locate\"></ion-icon>\n              </ion-button>\n              <ion-button href=\"/polylines\">\n                <ion-icon name=\"analytics\" alt=\"Polylines\"></ion-icon>\n              </ion-button>\n                <ion-button href=\"/home\">\n                  <ion-icon name=\"map\"></ion-icon>\n              </ion-button>\n              <ion-button href=\"/settings\" color=\"warning\">\n                <ion-icon name=\"settings-outline\"></ion-icon>\n              </ion-button>\n              <ion-button href=\"/profile\">\n                <ion-icon size=\"large\" name=\"person-circle\"></ion-icon>\n              </ion-button>\n            </ion-buttons>\n            <ion-title>\n              Ground Cover Mapper - Transects\n            </ion-title>\n          </ion-toolbar>\n        </ion-header>\n\n<ion-content>\n<ion-card >\n  <ion-item><h2>Settings</h2></ion-item>\n</ion-card>\n\n<ion-item>\n  <ion-label>Manager Controls</ion-label>\n  <ion-toggle [(ngModel)]=\"ismanager\" size=\"large\"></ion-toggle>\n</ion-item>\n<ion-item>\n  <ion-label>Your Maps</ion-label>\n  <ion-button >              <ion-badge color=\"success\">\n    44\n  </ion-badge></ion-button>\n</ion-item>\n<ion-item>\n  <ion-label>Your Team</ion-label>\n  <ion-button >              <ion-badge color=\"success\">\n    3\n  </ion-badge></ion-button>\n</ion-item>\n<ion-item>\n  <ion-label>Schedule</ion-label>\n  <ion-button >              <ion-badge color=\"success\">\n    Next event in 2 days\n  </ion-badge></ion-button>\n</ion-item>\n\n\n<ion-item>\n  <ion-label>Download Map Data</ion-label>\n  <ion-button > \n    <ion-icon href=\"/\" name=\"download\" color=\"warning\" size=\"large\"></ion-icon>\n\n  </ion-button>\n</ion-item>\n\n<ion-item>\n  <my-select [(ngModel)]=\"whatever\" [list]=\"whateverList\" ngDefaultControl></my-select>\n</ion-item>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_settings_settings_module_ts-es2015.js.map