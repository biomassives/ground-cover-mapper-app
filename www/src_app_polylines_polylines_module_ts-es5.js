(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkGround_Cover_Mappper"] = self["webpackChunkGround_Cover_Mappper"] || []).push([["src_app_polylines_polylines_module_ts"], {
    /***/
    36355:
    /*!*******************************************************!*\
      !*** ./src/app/polylines/polylines-routing.module.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PolylinesPageRoutingModule": function PolylinesPageRoutingModule() {
          return (
            /* binding */
            _PolylinesPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _polylines_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./polylines.page */
      69616);

      var routes = [{
        path: '',
        component: _polylines_page__WEBPACK_IMPORTED_MODULE_0__.PolylinesPage
      }];

      var _PolylinesPageRoutingModule = /*#__PURE__*/_createClass(function PolylinesPageRoutingModule() {
        _classCallCheck(this, PolylinesPageRoutingModule);
      });

      _PolylinesPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _PolylinesPageRoutingModule);
      /***/
    },

    /***/
    33584:
    /*!***********************************************!*\
      !*** ./src/app/polylines/polylines.module.ts ***!
      \***********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PolylinesPageModule": function PolylinesPageModule() {
          return (
            /* binding */
            _PolylinesPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _polylines_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./polylines.page */
      69616);
      /* harmony import */


      var _polylines_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./polylines-routing.module */
      36355);

      var _PolylinesPageModule = /*#__PURE__*/_createClass(function PolylinesPageModule() {
        _classCallCheck(this, PolylinesPageModule);
      });

      _PolylinesPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _polylines_routing_module__WEBPACK_IMPORTED_MODULE_1__.PolylinesPageRoutingModule],
        declarations: [_polylines_page__WEBPACK_IMPORTED_MODULE_0__.PolylinesPage]
      })], _PolylinesPageModule);
      /***/
    },

    /***/
    69616:
    /*!*********************************************!*\
      !*** ./src/app/polylines/polylines.page.ts ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PolylinesPage": function PolylinesPage() {
          return (
            /* binding */
            _PolylinesPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_polylines_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./polylines.page.html */
      95205);
      /* harmony import */


      var _polylines_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./polylines.page.scss */
      21263);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! leaflet */
      64216);
      /* harmony import */


      var leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var leaflet_draw__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! leaflet-draw */
      93787);
      /* harmony import */


      var leaflet_draw__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet_draw__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      87152);

      var _PolylinesPage = /*#__PURE__*/function () {
        function PolylinesPage(geolocation) {
          _classCallCheck(this, PolylinesPage);

          this.geolocation = geolocation;
          this.drawnItems = new leaflet__WEBPACK_IMPORTED_MODULE_2__.FeatureGroup();
          this.features = {
            base: new leaflet__WEBPACK_IMPORTED_MODULE_2__.FeatureGroup(),
            layers: new leaflet__WEBPACK_IMPORTED_MODULE_2__.FeatureGroup()
          };
        }

        _createClass(PolylinesPage, [{
          key: "initDraw",
          value: function initDraw() {
            //this.drawItems is a L.FeatureGroup(), used to store polygon
            this.map.addLayer(this.drawnItems);
            this.drawControl = new leaflet__WEBPACK_IMPORTED_MODULE_2__.Control.Draw({
              //Hide the drawn toolbar, because my project does not need to be drawn manually 
              edit: {
                featureGroup: this.drawnItems,
                remove: false
              }
            }); //hide edit handlers tip

            leaflet__WEBPACK_IMPORTED_MODULE_2__.drawLocal.edit.handlers.edit.tooltip.text = null;
            leaflet__WEBPACK_IMPORTED_MODULE_2__.drawLocal.edit.handlers.edit.tooltip.subtext = null;
            this.map.addControl(this.drawControl);
          }
        }, {
          key: "result",
          value: function result() {
            var _this = this;

            this.geolocation.getCurrentPosition().then(function (resp) {
              console.log(resp.coords.latitude);
              console.log(resp.coords.longitude);

              _this.leafletMap(resp.coords.latitude, resp.coords.longitude);
            })["catch"](function (error) {
              console.log('Error getting location', error);
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.result();
          }
        }, {
          key: "leafletMap",
          value: function leafletMap(lat, _long) {
            this.map = leaflet__WEBPACK_IMPORTED_MODULE_2__.map('mapId', {
              center: [lat, _long],
              zoom: 17
            });
            leaflet__WEBPACK_IMPORTED_MODULE_2__.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: 'OpenStreetMap.org'
            }).addTo(this.map);
            var ondeestou = leaflet__WEBPACK_IMPORTED_MODULE_2__.icon({
              iconUrl: '../../assets/ondeestou.png',
              iconSize: [38, 52],
              iconAnchor: [20, 40],
              popupAnchor: [-3, -40]
            });
            var content = document.querySelector('.leaflet-draw-edit-edit');
            content.click();

            this.drawControl._toolbars['edit'].enable();

            leaflet__WEBPACK_IMPORTED_MODULE_2__.marker({
              lat: 39.9593458,
              lng: -105.5122808
            }, {
              icon: ondeestou,
              draggable: true
            }).addTo(this.map).bindPopup('Hi!').on('dragend', function (event) {
              var marker = event.target;
              var position = marker.getLatLng();
              alert(position);
              marker.setLatLng(new leaflet__WEBPACK_IMPORTED_MODULE_2__.LatLng(position.lat, position.lng), {
                draggable: 'true'
              });
            });
          }
        }, {
          key: "drawPolygon",
          value: function drawPolygon() {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var layer, options;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      layer = leaflet__WEBPACK_IMPORTED_MODULE_2__.polygon([[31.4, 115.7], [31.5, 117.1], [29.5, 117.1], [29.5, 115.1]], {
                        weight: 1,
                        color: '#3366ff',
                        opacity: 1,
                        fillColor: '#3366ff',
                        fillOpacity: 1
                      });
                      this.drawnItems.addLayer(layer);
                      leaflet__WEBPACK_IMPORTED_MODULE_2__.rectangle([[39.9593448, -105.5122818], [39.9593418, -105.5122828]], {
                        color: 'red'
                      }).addTo(this.map).bindPopup('chinga');
                      leaflet__WEBPACK_IMPORTED_MODULE_2__.polygon([[39.9596648, -105.511901], [39.9594, -105.51453], [39.958142, -105.515088], [39.958488, -105.51]], {
                        color: 'red'
                      }).addTo(this.map).bindPopup('chinga');
                      options = {
                        radius: 300,
                        color: 'red',
                        fillColor: 'red',
                        fillOpacity: 0.2
                      };
                      leaflet__WEBPACK_IMPORTED_MODULE_2__.circle({
                        lat: -23.9700323,
                        lng: -46.3159908
                      }, options).addTo(this.map);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.map.remove();
          }
        }]);

        return PolylinesPage;
      }();

      _PolylinesPage.ctorParameters = function () {
        return [{
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__.Geolocation
        }];
      };

      _PolylinesPage = (0, tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-home',
        template: _raw_loader_polylines_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_polylines_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _PolylinesPage);
      /***/
    },

    /***/
    21263:
    /*!***********************************************!*\
      !*** ./src/app/polylines/polylines.page.scss ***!
      \***********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 30%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBvbHlsaW5lcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUFBRjs7QUFHQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBRUEsY0FBQTtFQUVBLFNBQUE7QUFGRjs7QUFLQTtFQUNFLHFCQUFBO0FBRkYiLCJmaWxlIjoicG9seWxpbmVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAzMCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cblxuI2NvbnRhaW5lciBzdHJvbmcge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4jY29udGFpbmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuXG4gIGNvbG9yOiAjOGM4YzhjO1xuXG4gIG1hcmdpbjogMDtcbn1cblxuI2NvbnRhaW5lciBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufSJdfQ== */";
      /***/
    },

    /***/
    95205:
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/polylines/polylines.page.html ***!
      \*************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons collapse=\"true\" slot=\"end\">\n      <ion-button href=\"/addpoint\">\n        <ion-icon name=\"locate\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/polylines\" color=\"warning\">\n        <ion-icon name=\"analytics\" alt=\"Polylines\" color=\"warning\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/home\">\n        <ion-icon name=\"map\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/settings\">\n        <ion-icon name=\"settings-outline\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/profile\">\n        <ion-icon size=\"large\" name=\"person-circle\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Ground Cover Mapper - Transects\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <div id=\"mapId\" style=\"width: 100%; height: 100%\"></div>\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_polylines_polylines_module_ts-es5.js.map