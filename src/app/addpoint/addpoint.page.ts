import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-addpoint',
  templateUrl: './addpoint.page.html',
  styleUrls: ['./addpoint.page.scss'],
})
export class AddpointPage implements OnInit {
  ionicForm: FormGroup;
  defaultDate = "2020-04-01";
  isSubmitted = false;
  constructor(public formBuilder: FormBuilder) { }
  ngOnInit() {

    this.ionicForm = this.formBuilder.group({
      label: ['', [Validators.required, Validators.minLength(2)]],
      dob: [this.defaultDate],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }
  getDate(e): void {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.ionicForm.get('dob').setValue(date, {
      onlyself: true
    })
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)
    /*  this.ionicForm.reset(); */
    }
  }

}
