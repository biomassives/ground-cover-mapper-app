import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-draw';
import 'leaflet';
import { antPath } from 'leaflet-ant-path';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'polylines.page.html',
  styleUrls: ['polylines.page.scss'],
})
export class PolylinesPage implements OnInit, OnDestroy{
  
  map: L.Map;


  initDraw(){
    //this.drawItems is a L.FeatureGroup(), used to store polygon
     this.map.addLayer(this.drawnItems);
     this.drawControl = new L.Control.Draw({
      //Hide the drawn toolbar, because my project does not need to be drawn manually 
       edit: {
           featureGroup: this.drawnItems,
           remove: false,
       }
     });
 
    //hide edit handlers tip
     L.drawLocal.edit.handlers.edit.tooltip.text = null;
     L.drawLocal.edit.handlers.edit.tooltip.subtext = null;
     this.map.addControl(this.drawControl);
   }


  private drawnItems = new L.FeatureGroup();
  private drawControl;
  private features = {
    base: new L.FeatureGroup(),
    layers: new L.FeatureGroup()
  };

  constructor( private geolocation: Geolocation) {}

  Var_latitude: '';
  Var_longitude: '';

  result() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        console.log(resp.coords.latitude);
        console.log(resp.coords.longitude);

        this.leafletMap(resp.coords.latitude,resp.coords.longitude)
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
  }

  ngOnInit() {}

  ionViewDidEnter() 
  { 
    this.result();     
  }


  leafletMap(lat,long) {
    this.map = L.map('mapId', {
      center: [lat,  long],
      zoom: 17
    })

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'OpenStreetMap.org',
    }).addTo(this.map)

    const ondeestou = L.icon({
      iconUrl: '../../assets/ondeestou.png',
      iconSize: [38, 52],
      iconAnchor: [20, 40], 
      popupAnchor: [-3, -40]
    });

 
    let content = document.querySelector('.leaflet-draw-edit-edit') as HTMLElement
    content.click();
   this.drawControl._toolbars['edit'].enable();
  
   L.marker({ lat: 39.9593458, lng: -105.5122808 }, { icon: ondeestou, draggable: true }).addTo(this.map).bindPopup('Hi!').on('dragend', function(event){
    var marker = event.target;
    var position = marker.getLatLng();
    alert ( position );
    marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
    });

  }

  private async drawPolygon(){
    let layer = L.polygon([[31.4, 115.7], [31.5, 117.1], [29.5, 117.1], [29.5, 115.1]], {
      weight: 1,
      color:'#3366ff',
      opacity: 1,
      fillColor:'#3366ff',
      fillOpacity: 1,
    });

    this.drawnItems.addLayer(layer);
  

 



    L.rectangle( [[39.9593448, -105.5122818], [39.9593418, -105.5122828]] , {color: 'red'}).addTo(this.map).bindPopup('chinga');
    L.polygon( [[39.9596648, -105.511901], [39.9594, -105.51453], [39.958142, -105.515088], [39.958488, -105.51]] , {color: 'red'}).addTo(this.map).bindPopup('chinga');
   
 
    const options = {
      radius: 300,
      color: 'red',
      fillColor: 'red',
      fillOpacity: 0.2
    }

    L.circle({ lat: -23.9700323, lng:  -46.3159908 }, options).addTo(this.map);
  }

  ngOnDestroy() {
    this.map.remove();
  }

}
