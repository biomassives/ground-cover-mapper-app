import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { PolylinesPage } from './polylines.page';

import { PolylinesPageRoutingModule } from './polylines-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PolylinesPageRoutingModule
  ],
  declarations: [PolylinesPage]
})
export class PolylinesPageModule {}
