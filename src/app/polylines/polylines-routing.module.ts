import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PolylinesPage } from './polylines.page';

const routes: Routes = [
  {
    path: '',
    component: PolylinesPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PolylinesPageRoutingModule {}
