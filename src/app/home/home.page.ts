import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-draw';
import { antPath } from 'leaflet-ant-path';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy{
  
  map: L.Map;

  constructor( private geolocation: Geolocation) {}

  Var_latitude: 39.9593458;
  Var_longitude: -105.5122808;

  result() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        console.log(resp.coords.latitude);
        console.log(resp.coords.longitude);

        this.leafletMap(resp.coords.latitude,resp.coords.longitude)
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
  }

  ngOnInit() {}

  ionViewDidEnter() 
  { 
    this.result();     
  }


  leafletMap(lat,long) {
    this.map = L.map('mapId', {
      center: [lat,  long],
      zoom: 17
    })

    L.EditToolbar; 

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'OpenStreetMap.org',
    }).addTo(this.map)

    const ondeestou = L.icon({
      iconUrl: '../../assets/ondeestou.png',
      iconSize: [38, 52],
      iconAnchor: [20, 40], 
      popupAnchor: [-3, -40]
    });

  	var drawnItems = new L.FeatureGroup();
		this.map.addLayer(drawnItems);

		var drawControl = new L.Control.Draw({
			position: 'topright',
			draw: {
				polygon: {
					shapeOptions: {
						color: 'purple'
					},
					allowIntersection: false,
					drawError: {
						color: 'orange',
						timeout: 1000
					},
					showArea: true,
					metric: false,
					repeatMode: true
				},
				polyline: {
					shapeOptions: {
						color: 'red'
					},
				},
				circle: {
					shapeOptions: {
						color: 'steelblue'
					},
				},
				marker: {
					icon: ondeestou
				},
			},
			edit: {
				featureGroup: drawnItems
			}
		});
		this.map.addControl(drawControl);

    this.map.on('draw:created', function (e) {
			var type = e.propagatedFrom,
				layer = e.propagatedFrom;

			if (type === 'marker') {
				layer.bindPopup('A popup!');
			}

			drawnItems.addLayer(layer);
		});


    L.marker({ lat: 39.9593458, lng: -105.5122808 }, { icon: ondeestou, draggable: true }).addTo(this.map).bindPopup('<ion-toggle color="light"></ion-toggle> <ion-toggle color="dark"></ion-toggle>').on('dragend', function(event){
      var marker = event.target;
      var position = marker.getLatLng();
      alert ( position );
      marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
    });



    new L.Polygon( [[39.959861, -105.516042], [39.960404, -105.512921], [39.9593458, -105.5122808], [39.9593448, -105.5122818], [39.9593418, -105.5122828], [39.9593438, -105.5122808]] , {color: 'red' }).addTo(this.map).bindPopup('chiww111nga');
   
   
    const options = {
      radius: 300,
      color: 'red',
      fillColor: 'red',
      fillOpacity: 0.2
    }

    L.circle({ lat: -23.9700323, lng:  -46.3159908 }, options).addTo(this.map);
  }

  ngOnDestroy() {
    this.map.remove();
  }

}

